# the communicator data
communicator_data = {
    'radio': {
        'groupID': 1.0,
        'receivedSignal': 0
    },
    'mode': {
        'changeChannelMode': False
    },
    'received': {
        'ID': 0
    }
}

# sound effects
communicator_sounds = {
    'beep': {
        'tone': Note.C5,
        'speed': music.beat(BeatFraction.EIGHTH)
    },
    'call': {
        'tone': Note.G
    }
}

def changeChannelMode(adjustIncrement = 0):
    """
        changeChannelMode allows the changing of radio channels.

        Parameters:
            adjustIncrement: (integer) how much to adjust
        Returns:
            groupID: (integer) the current channel ID
    """

    # Enable the change channel and ID mode.
    communicator_data['mode']['changeChannelMode'] = True

    # The groupID variable includes the channel number (integer) and the device's identifier (non-integer). 
    if (adjustIncrement):
        if (adjustIncrement >= 1):
            # Increase the selected ID number!
            communicator_data['radio']['groupID'] += 0.1
        elif (adjustIncrement <= -1) and (communicator_data['radio']['groupID'] > 1):
            # Decrease the selected ID number!
            communicator_data['radio']['groupID'] = communicator_data['radio']['groupID'] - 0.1
        
        # Get device channel only. 
        device_groupID = str(communicator_data['radio']['groupID'])
        device_channelNumber = int(device_groupID.split('.')[0])

        # Set the group number properly. 
        radio.set_group(device_channelNumber)
    
    # Stop the animation and show the number. 
    led.stop_animation()
    basic.show_number(communicator_data['radio']['groupID'])

def message_send(): 
    """
        Send the message. 

        Parameters: none
        Returns: none
    """

    # Convert
    device_groupID = str(communicator_data['radio']['groupID'])

    # Get the device number only. 
    device_number = int((device_groupID.split("."))[1])

    # Broadcast the message. 
    radio.send_number(device_number)

def message_receive():
    """
        Display the received message. 

        Parameters: none
        Returns: none
    """

    # Get the device number only.
    device_receive_number = communicator_data['received']['ID']

    # Display the device number. 
    basic.show_number(device_receive_number)

    # Ring.
    music.ring_tone(communicator_sounds['call']['tone'])

    # Pause. 
    basic.pause(5000)

    # Stop. 
    basic.clear_screen()
    music.stop_all_sounds()

def on_button_pressed_ab():
    # Play the button clicked sound.
    music.play_tone(communicator_sounds['beep']['tone'], communicator_sounds['beep']['speed'])
    
    # Stop the animation. 
    led.stop_animation()
    
    # allow the toggling of channel changing
    if not(communicator_data['mode']['changeChannelMode']):
        changeChannelMode()
    
    else:
        communicator_data['mode']['changeChannelMode'] = False
        basic.clear_screen()

def on_button_pressed_a():
    # Play the button clicked sound.
    music.play_tone(communicator_sounds['beep']['tone'], communicator_sounds['beep']['speed'])
    
    # Stop the animation.
    led.stop_animation()
   
    if (communicator_data['mode']['changeChannelMode']):
        changeChannelMode(-1)
    else:
        # A button is mapped to short sounds.
        message_send()

def on_button_pressed_b():
    # Play the button clicked sound.
    music.play_tone(communicator_sounds['beep']['tone'], communicator_sounds['beep']['speed'])
        
    # Stop the animation.
    led.stop_animation()
    
    if (communicator_data['mode']['changeChannelMode']):
        changeChannelMode(1)
    else:
        # B button is mapped to the longer sound
        message_send()

input.on_button_pressed(Button.AB, on_button_pressed_ab)
input.on_button_pressed(Button.A, on_button_pressed_a)
input.on_button_pressed(Button.B, on_button_pressed_b)

def on_received_number(receivedNumber):
    communicator_data['received']['ID'] = receivedNumber
    message_receive()

radio.on_received_number(on_received_number)