//  the communicator data
let communicator_data = {
    "radio" : {
        "groupID" : 1.0,
        "receivedSignal" : 0,
    }
    ,
    "mode" : {
        "changeChannelMode" : false,
    }
    ,
    "received" : {
        "ID" : 0,
    }
    ,
}

//  sound effects
let communicator_sounds = {
    "beep" : {
        "tone" : Note.C5,
        "speed" : music.beat(BeatFraction.Eighth),
    }
    ,
    "call" : {
        "tone" : Note.G,
    }
    ,
}

function changeChannelMode(adjustIncrement: number = 0) {
    let device_groupID: string;
    let device_channelNumber: number;
    /** 
        changeChannelMode allows the changing of radio channels.

        Parameters:
            adjustIncrement: (integer) how much to adjust
        Returns:
            groupID: (integer) the current channel ID
    
 */
    //  Enable the change channel and ID mode.
    communicator_data["mode"]["changeChannelMode"] = true
    //  The groupID variable includes the channel number (integer) and the device's identifier (non-integer). 
    if (adjustIncrement) {
        if (adjustIncrement >= 1) {
            //  Increase the selected ID number!
            communicator_data["radio"]["groupID"] += 0.1
        } else if (adjustIncrement <= -1 && communicator_data["radio"]["groupID"] > 1) {
            //  Decrease the selected ID number!
            communicator_data["radio"]["groupID"] = communicator_data["radio"]["groupID"] - 0.1
        }
        
        //  Get device channel only. 
        device_groupID = "" + communicator_data["radio"]["groupID"]
        device_channelNumber = parseInt(_py.py_string_split(device_groupID, ".")[0])
        //  Set the group number properly. 
        radio.setGroup(device_channelNumber)
    }
    
    //  Stop the animation and show the number. 
    led.stopAnimation()
    basic.showNumber(communicator_data["radio"]["groupID"])
}

function message_send() {
    /** 
        Send the message. 

        Parameters: none
        Returns: none
    
 */
    //  Convert
    let device_groupID = "" + communicator_data["radio"]["groupID"]
    //  Get the device number only. 
    let device_number = parseInt(_py.py_string_split(device_groupID, ".")[1])
    //  Broadcast the message. 
    radio.sendNumber(device_number)
}

function message_receive() {
    /** 
        Display the received message. 

        Parameters: none
        Returns: none
    
 */
    //  Get the device number only.
    let device_receive_number = communicator_data["received"]["ID"]
    //  Display the device number. 
    basic.showNumber(device_receive_number)
    //  Ring.
    music.ringTone(communicator_sounds["call"]["tone"])
    //  Pause. 
    basic.pause(5000)
    //  Stop. 
    basic.clearScreen()
    music.stopAllSounds()
}

input.onButtonPressed(Button.AB, function on_button_pressed_ab() {
    //  Play the button clicked sound.
    music.playTone(communicator_sounds["beep"]["tone"], communicator_sounds["beep"]["speed"])
    //  Stop the animation. 
    led.stopAnimation()
    //  allow the toggling of channel changing
    if (!communicator_data["mode"]["changeChannelMode"]) {
        changeChannelMode()
    } else {
        communicator_data["mode"]["changeChannelMode"] = false
        basic.clearScreen()
    }
    
})
input.onButtonPressed(Button.A, function on_button_pressed_a() {
    //  Play the button clicked sound.
    music.playTone(communicator_sounds["beep"]["tone"], communicator_sounds["beep"]["speed"])
    //  Stop the animation.
    led.stopAnimation()
    if (communicator_data["mode"]["changeChannelMode"]) {
        changeChannelMode(-1)
    } else {
        //  A button is mapped to short sounds.
        message_send()
    }
    
})
input.onButtonPressed(Button.B, function on_button_pressed_b() {
    //  Play the button clicked sound.
    music.playTone(communicator_sounds["beep"]["tone"], communicator_sounds["beep"]["speed"])
    //  Stop the animation.
    led.stopAnimation()
    if (communicator_data["mode"]["changeChannelMode"]) {
        changeChannelMode(1)
    } else {
        //  B button is mapped to the longer sound
        message_send()
    }
    
})
radio.onReceivedNumber(function on_received_number(receivedNumber: number) {
    communicator_data["received"]["ID"] = receivedNumber
    message_receive()
})
